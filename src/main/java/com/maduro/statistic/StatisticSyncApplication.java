package com.maduro.statistic;

import java.time.Duration;
import java.time.Instant;

import com.maduro.statistic.service.MainSyncService;

public class StatisticSyncApplication {

	public static void main(String[] args) {

		try {
			System.out.println("------------------- sync");
			Instant start = Instant.now();

			new MainSyncService().process("/home/maduro/pokerstas-files/all-in-statistic/all-in.csv");

			System.out.println("done: " + Duration.between(start, Instant.now()).toSeconds() + " sec");

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}
