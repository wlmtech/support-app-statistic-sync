package com.maduro.statistic.domain;

public enum CriticalHandOutcomeEnum {
	TIED, BEST_WIN, WORST_WIN
}
