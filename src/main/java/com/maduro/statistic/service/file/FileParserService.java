package com.maduro.statistic.service.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.maduro.statistic.domain.HandDataModel;
import com.maduro.statistic.service.file.exception.FileCannotBeDirectoryException;
import com.maduro.statistic.service.file.exception.FileNotExistsException;
import com.maduro.statistic.service.file.exception.FileNotProvidedException;
import com.maduro.statistic.service.file.exception.FileTooBigException;
import com.maduro.statistic.util.Utils;

public class FileParserService {

	public FileParserServiceDTO process(String filePath) throws Exception {

		FileParserServiceDTO fileParserServiceDTO = new FileParserServiceDTO();

		final String HEAD = "\"game\"";

		try {

			BufferedReader br = new BufferedReader(new FileReader(getFileFromPath(filePath)));

			String line;
			while ((line = br.readLine()) != null) {
				if (line.startsWith(HEAD)) {
					continue;
				}
				HandDataModel handDataModel = parseLine(line);
				fileParserServiceDTO.addHandDataModel(handDataModel);
			}

			br.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return fileParserServiceDTO;
	}

	private File getFileFromPath(String filePath) {

		final long MAX_SIZE = 100 * 1000;

		if (filePath == null || filePath.isBlank()) {
			throw new FileNotProvidedException();
		}

		File file = new File(filePath);

		if (!file.exists()) {
			throw new FileNotExistsException();
		}
		if (file.isDirectory()) {
			throw new FileCannotBeDirectoryException();
		}
		if (file.length() > MAX_SIZE) {
			throw new FileTooBigException("File cannot be greater than " + MAX_SIZE + " bytes");
		}

		return file;
	}

	private HandDataModel parseLine(String line) throws Exception {
		String[] fieldsFromFile = line.split(",");
		return (HandDataModel) Utils.setObjectFieldFromArray(HandDataModel.class, fieldsFromFile);
	};

}
