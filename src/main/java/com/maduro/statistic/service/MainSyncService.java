package com.maduro.statistic.service;

import com.maduro.statistic.service.evaluator.HandEvaluatorService;
import com.maduro.statistic.service.evaluator.HandEvaluatorServiceDTO;
import com.maduro.statistic.service.file.FileParserService;
import com.maduro.statistic.service.file.FileParserServiceDTO;
import com.maduro.statistic.service.mapper.HandMapperService;
import com.maduro.statistic.service.mapper.HandMapperServiceDTO;
import com.maduro.statistic.service.view.StatisticViewService;

public class MainSyncService {

	public void process(String filePath) throws Exception {

		FileParserServiceDTO fileParserServiceDTO = 
				new FileParserService()
					.process(filePath);
		
		HandMapperServiceDTO handMapperServiceDTO = 
				new HandMapperService()
					.process(fileParserServiceDTO);
				
		HandEvaluatorServiceDTO handEvaluatorServiceDTO = 
				new HandEvaluatorService()
					.process(handMapperServiceDTO);

		StatisticViewService
			.builder()
			.build()
			.process(handEvaluatorServiceDTO)
			.showResult();

	}

}
