package com.maduro.statistic.service.view;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.maduro.statistic.domain.CriticalHandOutcomeEnum;
import com.maduro.statistic.domain.HandDataModel;
import com.maduro.statistic.service.evaluator.HandEvaluatorServiceDTO;

import lombok.Builder;

@Builder
public class StatisticViewService {

	private final Map<String, List<HandDataModel>> tiedHandsMap = new TreeMap<String, List<HandDataModel>>();
	private final Map<String, List<HandDataModel>> bestHandsWinMap = new TreeMap<String, List<HandDataModel>>();
	private final Map<String, List<HandDataModel>> worstHandsWinMap = new TreeMap<String, List<HandDataModel>>();

	public StatisticViewService process(HandEvaluatorServiceDTO handEvaluatorServiceDTO) {
		if (handEvaluatorServiceDTO != null) {
			handEvaluatorServiceDTO.getHandDataModelMapParserDTOList().stream()
					.forEach(handDataModelMapParserDTO -> {
						this.addHand(handDataModelMapParserDTO.getGameCode(),
								handDataModelMapParserDTO.getHandDataModelList(),
								handDataModelMapParserDTO.getCriticalHandOutcomeEnum());
					});
		}
		return this;
	}

	private void addHand(String key, List<HandDataModel> value, CriticalHandOutcomeEnum criticalHandOutcomeEnum) {

		switch (criticalHandOutcomeEnum) {
		case BEST_WIN: {
			this.bestHandsWinMap.put(key, value);
			break;
		}
		case WORST_WIN: {
			this.worstHandsWinMap.put(key, value);
			break;
		}
		case TIED: {
			this.tiedHandsMap.put(key, value);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + criticalHandOutcomeEnum);
		}

	}

	public void showResult() {
		System.out.println("total tied: " + this.tiedHandsMap.size());
		System.out.println("total best: " + this.bestHandsWinMap.size());
		System.out.println("total worse: " + this.worstHandsWinMap.size());
	}
}