package com.maduro.statistic.service.evaluator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.maduro.statistic.domain.CriticalHandOutcomeEnum;
import com.maduro.statistic.domain.HandDataModel;
import com.maduro.statistic.service.mapper.HandMapperServiceDTO;
import com.yg2288.service.HandRankingService;

public class HandEvaluatorService {

	private enum AggressivityBehaviorEnum {
		RAISER, CALLER
	}

	public HandEvaluatorServiceDTO process(HandMapperServiceDTO handMapperServiceDTO)
			throws Exception {
		
		if (handMapperServiceDTO == null 
				|| handMapperServiceDTO.getHandDataModelMap() == null)
			return null;

		HandEvaluatorServiceDTO handEvaluatorServiceDTO = new HandEvaluatorServiceDTO();

		Map<String, List<HandDataModel>> handDataModelMap = handMapperServiceDTO.getHandDataModelMap();

		for (Map.Entry<String, List<HandDataModel>> entry : handDataModelMap.entrySet()) {

			if (entry.getValue().size() != 2) {
				continue;
			}

			if (!entry.getValue().get(0).getAll_in_action_street().equals("PRE_FLOP")) {
				continue;
			}

			String userName = "wmaduro";
			if (!checkIfPlayerExists(userName, entry.getValue())) {
				continue;
			}

			AggressivityBehaviorEnum aggressivityBehaviorEnum = AggressivityBehaviorEnum.RAISER;
			if (!checkAggressityBehavior(userName, aggressivityBehaviorEnum, entry.getValue())) {
//				System.out.println(
//						"ignored : player/aggressivity " + userName + "/" + aggressivityBehaviorEnum + " is not found");
				continue;
			}

			CriticalHandOutcomeEnum criticalHandOutcomeEnum = new HandDataService().analyzeHands(entry.getValue());
			handEvaluatorServiceDTO.addHandDataModelMap(entry.getKey(), entry.getValue(),
					criticalHandOutcomeEnum);

		}
		return handEvaluatorServiceDTO;
	}

	private static boolean checkIfPlayerExists(String playerName, List<HandDataModel> handDataModelList) {
		if (playerName == null) {
			return true;
		}
		return handDataModelList.stream().anyMatch(handDataModel -> handDataModel.getUser_name().equals(playerName));
	}

	private static boolean checkAggressityBehavior(String playerName, AggressivityBehaviorEnum aggressivityBehaviorEnum,
			List<HandDataModel> handDataModelList) {

		if (playerName == null || aggressivityBehaviorEnum == null) {
			return true;
		}

		String CALL = "CALL";
		String RAISE = "RAISE";
		String actionPreflop = (aggressivityBehaviorEnum.equals(AggressivityBehaviorEnum.CALLER)) ? CALL : RAISE;
		return handDataModelList.stream().anyMatch(handDataModel -> handDataModel.getUser_name().equals(playerName)
				&& handDataModel.getAction_pre_flop().equals(actionPreflop));
	}

	private class HandDataService {

		public CriticalHandOutcomeEnum analyzeHands(List<HandDataModel> listHandDataModel) throws Exception {

			List<HandDataModel> winnerHandsList = getWinnerHands(listHandDataModel);

			if (winnerHandsList.size() > 1) {
				return CriticalHandOutcomeEnum.TIED;
			} else {

				if (winnerHandsList.size() == 0) {
					throw new Exception("Nenhuma mao vencedora foi encontrada...");
				}

				String winnerHandCards = winnerHandsList.get(0).getCard_sequence();
				String[] cardsArray = getCards(listHandDataModel);
				String bestCards = getBestCards(cardsArray);

				boolean bestHandWon = winnerHandCards.contentEquals(bestCards);

				return bestHandWon ? CriticalHandOutcomeEnum.BEST_WIN : CriticalHandOutcomeEnum.WORST_WIN;
			}
		}

		private String getBestCards(String[] cardsArray) throws Exception {

			HandRankingService handRankingService = new HandRankingService();
			String bestHand = handRankingService.getBestStringHand(Arrays.asList(cardsArray));

			return bestHand;
		}

		private String[] getCards(List<HandDataModel> listHandDataModel) {

			ArrayList<String> returnList = new ArrayList<String>();
			listHandDataModel.stream().forEach(handDataModel -> {
				String cards = handDataModel.getCard_sequence();
				returnList.add(cards);
			});

			return returnList.toArray(new String[returnList.size()]);
		}

		private List<HandDataModel> getWinnerHands(List<HandDataModel> listHandDataModel) {

			List<HandDataModel> listWinnerHands = new ArrayList<>();

			Double[] lastValue = { 0. };
			listHandDataModel.forEach(handDataModel -> {

				Double valueWonParsed = handDataModel.parseValueWonToDouble();

				if (valueWonParsed != 0) {
					if (valueWonParsed < lastValue[0]) {
						return;
					}

					if (valueWonParsed > lastValue[0]) {
						listWinnerHands.clear();
					}
					listWinnerHands.add(handDataModel);
					lastValue[0] = valueWonParsed;

				}

			});
			return listWinnerHands;
		}
	}
}
