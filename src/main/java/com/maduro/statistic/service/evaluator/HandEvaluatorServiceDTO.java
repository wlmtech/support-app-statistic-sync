package com.maduro.statistic.service.evaluator;

import java.util.ArrayList;
import java.util.List;

import com.maduro.statistic.domain.CriticalHandOutcomeEnum;
import com.maduro.statistic.domain.HandDataModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

public class HandEvaluatorServiceDTO {

	@AllArgsConstructor
	@Data
	public class HandDataModelMapParserDTO {
		private String gameCode;
		private List<HandDataModel> handDataModelList;
		private CriticalHandOutcomeEnum criticalHandOutcomeEnum;
	}

	@Getter
	private List<HandDataModelMapParserDTO> handDataModelMapParserDTOList = new ArrayList<>();

	public void addHandDataModelMap(String gameCode, List<HandDataModel> handDataModelList,
			CriticalHandOutcomeEnum criticalHandOutcomeEnum) {
		
		handDataModelMapParserDTOList
				.add(new HandDataModelMapParserDTO(gameCode, handDataModelList, criticalHandOutcomeEnum));

	}

}
